<?php

namespace Bravi\Http\Controllers;

use Illuminate\Http\Request;

class BalancedBracketsController extends Controller
{
    /**
     * Display a form for bracket test.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('application.brackets.index');
    }
}
