<?php

namespace Bravi\Http\Controllers;

use Illuminate\Http\Request;
use Bravi\Models\Person;
use Bravi\Models\Contact;
use Response;

class ApiController extends Controller
{
    protected $persons;
    protected $contacts;

    public function __construct(Person $persons, Contact $contacts)
    {
        $this->persons = $persons;
        $this->contacts = $contacts;
    }
    /**
     * Return all contacts registered.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $persons  = $this->persons->with('contacts')->get();
            if (!$persons->isEmpty()) {
                foreach ($persons as $person) {
                    $response[] = [
                        'id' => $person->id,
                        'name' => $person->name,
                        'email'=> $person->contacts->email,
                        'telephone'=> $person->contacts->telephone,
                        'cellphone'=> $person->contacts->cellphone,
                        'whatsapp'=> $person->contacts->whatsapp,
                    ];
                }
                return Response::json(
                    [
                        'status'=>'success',
                        'contacts'=>$response
                    ],
                    200
                );
            }
            return Response::json(
                [
                'status'=>'error',
                'message'=>'No contacts registered'
                ],
                400
            );
        } catch (Exception $e) {
            return Response::json(
                [
                    'status'=>'error',
                    'message'=>'An error has occurred, please try again!'
                ],
                500
            );
        }
    }

    /**
     * Store a new contact .
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $person = $this->persons->create(
                [
                    'name'=> $request->name,
                ]
            );

            $this->contacts->create(
                [
                    'people_id' => $person->id,
                    'email' => $request->email,
                    'telephone' => $request->telephone,
                    'cellphone' => $request->cellphone,
                    'whatsapp' => $request->whatsapp,
                ]
            );
            $response= [
                'id' => $person->id,
                'name' => $person->name,
                'email'=> $person->contacts->email,
                'telephone'=> $person->contacts->telephone,
                'cellphone'=> $person->contacts->cellphone,
                'whatsapp'=> $person->contacts->whatsapp,
            ];
            return Response::json(
                [
                   'status'=>'success',
                   'contacts'=>$response
                ],
                200
            );
        } catch (Exception $e) {
            return Response::json(
                [
                    'status'=>'error',
                    'message'=>'An error has occurred, please try again!'
                ],
                500
            );
        }
    }

    /**
     * Get the specified contact.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */

    public function show($id)
    {
        try {
            $person = $this->persons->find($id);
            if ($person) {
                $response= [
                    'id' => $person->id,
                    'name' => $person->name,
                    'email'=> $person->contacts->email,
                    'telephone'=> $person->contacts->telephone,
                    'cellphone'=> $person->contacts->cellphone,
                    'whatsapp'=> $person->contacts->whatsapp,
                ];
                return Response::json(
                    [
                       'status'=>'success',
                       'contacts'=>$response
                    ],
                    200
                );
            }
            return Response::json(
                [
                   'status'=>'error',
                   'message'=>'No Contacts found with this id'
                ],
                400
            );
        } catch (Exception $e) {
            return Response::json(
                [
                    'status'=>'error',
                    'message'=>'An error has occurred, please try again!'
                ],
                500
            );
        }
    }

    /**
     * Update the specified contact.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $person = $this->persons->find($id);
            if ($person) {
                $person->update(
                    [
                        'name'=> $request->name,
                    ]
                );
                $person->contacts->update(
                    [
                        'email' => $request->email,
                        'telephone' => $request->telephone,
                        'cellphone' => $request->cellphone,
                        'whatsapp' => $request->whatsapp,
                    ]
                );
               
                // I search person again to return result altered
                $person = $this->persons->find($id);
                $response= [
                    'id' => $person->id,
                    'name' => $person->name,
                    'email'=> $person->contacts->email,
                    'telephone'=> $person->contacts->telephone,
                    'cellphone'=> $person->contacts->cellphone,
                    'whatsapp'=> $person->contacts->whatsapp,
                ];
                return Response::json(
                    [
                       'status'=>'success',
                       'contacts'=>$response
                    ],
                    200
                );
            }
            return Response::json(
                [
                   'status'=>'error',
                   'message'=>'No Contacts found with this id'
                ],
                400
            );
        } catch (Exception $e) {
            return Response::json(
                [
                    'status'=>'error',
                    'message'=>'An error has occurred, please try again!'
                ],
                500
            );
        }
    }

    /**
     * Delete a specified contact .
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $person = $this->persons->find($id);
            if ($person) {
                $person->delete();
                $person->contacts->delete();
                
                return Response::json(
                    [
                       'status'=>'success',
                       'message'=>'Contact deleted successfully!'
                    ],
                    200
                );
            }

            return Response::json(
                [
                   'status'=>'error',
                   'message'=>'No Contacts found with this id'
                ],
                400
            );
        } catch (Exception $e) {
            return Response::json(
                [
                    'status'=>'error',
                    'message'=>'An error has occurred, please try again!'
                ],
                500
            );
        }
    }
}
