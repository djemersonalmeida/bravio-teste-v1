<?php

namespace Bravi\Http\Controllers;

use Illuminate\Http\Request;
use Bravi\Http\Requests\WeatherRequest;

class WeatherController extends Controller
{
    protected static $endpoint = "http://api.openweathermap.org/data/2.5/weather";
    private $weatherKey;
    
    public function __construct()
    {
        $this->weatherKey = env('WEATHER_KEY');
    }
    /**
     * Display a Weather form.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('application.weather.index');
    }
    
    /**
     * Get a weather.
     *
     * @param  \Illuminate\Http\WeatherRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function getWeather(WeatherRequest $request)
    {
        $url = ( self::$endpoint . "?q=".$request->city."&units=imperial&appid=".$this->weatherKey);
        $data = self::getCurl($url);
        return view('application.weather.index', compact('data'));
    }

    public static function getCurl($url)
    {
        if (function_exists('curl_exec')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0');
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $response = curl_exec($ch);
            curl_close($ch);
        }
        if (empty($response)) {
            $response = file_get_contents($url);
        }
         $data = json_decode($response);
         return $data;
    }
}
