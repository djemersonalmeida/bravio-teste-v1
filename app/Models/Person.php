<?php
namespace Bravi\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Person
 *
 * @property int $id
 * @property string $name
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $contacts
 *
 * @package Bravi\Models
 */
class Person extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $fillable = [
        'name'
    ];

    public function contacts()
    {
        return $this->belongsTo(\Bravi\Models\Contact::class, 'id', 'people_id');
    }
}
