<?php
namespace Bravi\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Contact
 *
 * @property int $id
 * @property int $people_id
 * @property string $email
 * @property string $telephone
 * @property string $cellphone
 * @property string $whatsapp
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \Bravi\Models\Person $person
 *
 * @package Bravi\Models
 */
class Contact extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
      'people_id' => 'int'
    ];

    protected $fillable = [
       'people_id',
       'email',
       'telephone',
       'cellphone',
       'whatsapp'
    ];

    public function person()
    {
        return $this->belongsTo(\Bravi\Models\Person::class, 'people_id');
    }
}
