# SOBRE O PROEJETO

3 Aplicações, sendo elas:  

- 1. Balanced Brackets
- 2. Weather in my city
- 3. Contact List - RESTFull simples

Utilizando:  

- Laravel última versão
- PSR2
- Vagrant para virtualização
- REST
- JSON
- [OpenWeather](https://openweathermap.org/api/) 



# SUBINDO A MÁQUINA VIRTUAL PARA TESTES

Necessário instalar [Vagrant](https://www.vagrantup.com/) e [VirtualBox](https://www.virtualbox.org/).
Após instalar os programas, siga os passos: 

- Habilitar virtualização na bios no computador 
- vi /pasta_do_projeto/docs/config.yaml
- Edite a linha 46, aponte para o caminho absoluto do projeto em sua máquina, salve.
- Copie o arquivo config.yaml para /pasta_do_projeto/docs/virtualization/puphpet
- Navegue até a pasta /pasta_do_projeto/docs/virtualization/
- Rode o comando vagrant -up
- Aponte o hosts da sua maquina para o IP :  192.168.56.115 bravi.dev.com

Após os passos acima, crie um arquivo .env na raiz do projeto, com as configurações: 
<br/>
APP_NAME=Bravi  
APP_ENV=local  
APP_KEY=base64:oZf+h0HbWYDy0IJBBBOBVdgPuRKPZqdxmEUYvOuofVY=  
APP_DEBUG=true  
APP_URL=http://localhost  
LOG_CHANNEL=stack    
<br/>
DB_CONNECTION=mysql  
DB_HOST=127.0.0.1  
DB_PORT=3306  
DB_DATABASE=bravi_desenv  
DB_USERNAME=root  
DB_PASSWORD=root  
<br/>
BROADCAST_DRIVER=log  
CACHE_DRIVER=file  
SESSION_DRIVER=file  
SESSION_LIFETIME=120  
QUEUE_DRIVER=database  
<br/>
REDIS_HOST=127.0.0.1  
REDIS_PASSWORD=null  
REDIS_PORT=6379  
<br/>
MAIL_DRIVER=smtp  
MAIL_HOST=smtp.mailtrap.io  
MAIL_PORT=2525  
MAIL_USERNAME=null  
MAIL_PASSWORD=null  
MAIL_ENCRYPTION=null  
</br>
PUSHER_APP_ID=  
PUSHER_APP_KEY=  
PUSHER_APP_SECRET=  
PUSHER_APP_CLUSTER=mt1  
<br/>
MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"  
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"  
WEATHER_KEY = 23b968dac1c640152f0be78f877dcf76  

Após os passos acima, rode os seguintes comandos, na ordem: 
<br/>

- cd pasta_do_projeto/docs/virtualization  
- vagrant ssh
- cd /var/www/bravi 
- composer install
- composer dumpautoload -o
- php artisan key:generate
- php artisan migrate

- O sistema está disponivel na url : bravi.dev.com

Usuário e senha para logar no sistema para testes: 
<br/>
- user: bravi@bravi.com 
- senha: 12345678
<br/>
- As Aplicações 1 e 2 estarão disponíveis após o login: 
<br/>

Aplicação 3:  
<br/>
# METODOS DA  API:
## GET CONTACTS
----
  Returns all contacts json data.

* **URL**
http://bravi.dev.com/api/contact
* **Method:**
   `GET`

*  **URL Params**
    None

* **Data Params**
   None

* **Success Response:**
   * **Code:** 200 
    **Content:** `{ 
            "status":"success", 
            "contacts": [{
                "id":
                "name": 
                "email":
                "telephone":
                "cellphone":
                "whatsapp":
            }]
            }`
 
* **Error Response:**
  * **Code:** 400 NOT FOUND   
    **Content:** `{ "status" : "error", "message":"No contacts registered" }`
   
   OR

  * **Code:** 500 Internal Server Error   
    **Content:** `{ "status" : "error", "message":"An error has occurred, please try again!" }`


## CREATE A CONTACT
----
* **URL**
http://bravi.dev.com/api/contact

* **Method:**
   `POST`

*  **URL Params**
   None

* **Data Params**
    ```x-www-form-urlencoded
        "name": 
        "email":
        "telephone":
        "cellphone":
        "whatsapp":
    ```

* **Success Response:**

   * **Code:** 200 
    **Content:** `{ 
            "status":"success", 
            "contacts": {
                "id":
                "name": 
                "email":
                "telephone":
                "cellphone":
                "whatsapp":
            }
            }`
* **Error Response:**
 
  * **Code:** 500 Internal Server Error   
    **Content:** `{ "status" : "error", "message":"An error has occurred, please try again!" }`

## UPDATE A CONTACT
----
* **URL**
http://bravi.dev.com/api/contact/:id

* **Method:**
   `PUT`

*  **URL Params**
   **Required:**
     `id=[integer]`

* **Data Params**
    ```x-www-form-urlencoded
        "name": 
        "email":
        "telephone":
        "cellphone":
        "whatsapp":
    ```

* **Success Response:**

   * **Code:** 200 
    **Content:** `{ 
            "status":"success", 
            "contacts": {
                "id":
                "name": 
                "email":
                "telephone":
                "cellphone":
                "whatsapp":
            }
            }`
* **Error Response:**

  * **Code:** 400 NOT FOUND   
    **Content:** `{ "status" : "error", "message":"No Contacts found with this id" }`

  OR

  * **Code:** 500 Internal Server Error   
    **Content:** `{ "status" : "error", "message":"An error has occurred, please try again!" }`

## GET CONTACT
----
  returns a specific contact according to id.

* **URL**
http://bravi.dev.com/api/contact/:id

* **Method:**
 
  `GET`

*  **URL Params**
   **Required:**
     `id=[integer]`

* **Data Params**
   None

* **Success Response:**
  * **Code:** 200  
    **Content:** `{ 
            "status":"success", 
            "contacts": {
                "id":
                "name": 
                "email":
                "telephone":
                "cellphone":
                "whatsapp":
            }
            }`
 
* **Error Response:**

  * **Code:** 400 NOT FOUND   
    **Content:** `{ "status" : "error", "message":"No Contacts found with this id" }`

  OR

  * **Code:** 500 Internal Server Error   
    **Content:** `{ "status" : "error", "message":"An error has occurred, please try again!" }`

## DELETE A CONTACT
----
  Delete a specific contact according to id.

* **URL**
http://bravi.dev.com/api/contact/:id

* **Method:**
 
  `DELETE`

*  **URL Params**
   **Required:**
     `id=[integer]`

* **Data Params**
   None

* **Success Response:**
  * **Code:** 200  
    **Content:**`{ "status" : "success", "message":"Contact deleted successfully!" }`
 
* **Error Response:**

  * **Code:** 400 NOT FOUND   
    **Content:** `{ "status" : "error", "message":"No Contacts found with this id" }`

  OR

  * **Code:** 500 Internal Server Error   
    **Content:** `{ "status" : "error", "message":"An error has occurred, please try again!" }`


