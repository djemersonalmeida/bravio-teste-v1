@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                 <a href="{{route('balanced_brackets')}}" class="btn btn-primary btn-lg btn-block padding">
                      Balanced Brackets
                 </a>
                  <a href="{{route('weather')}}" class="btn btn-primary btn-lg btn-block padding">
                    Weather in my city
                 </a>
        
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
