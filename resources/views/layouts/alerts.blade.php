@if (session('status'))
<div class="form-group col-md-12">
    <div class="alert alert-{{ session('class') ? session('class') : 'success' }}">
        {{ session('status') }}
    </div>
</div>
@endif
@if ($errors->any())
<div class="form-group col-md-12">
	<div class="alert alert-danger ">
	    <ul>
			@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif