@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Balanced Brackets</div>

                <div class="card-body">
                    <div class="form-group">
                        <input type="text" class="form-control" name="parens" id="brackets"  placeholder="" required>
                    </div>
                    <button id="btn" class="btn btn-block btn-primary">Calculate</button>

                </div>
                <div class="form-group">
                    <div class="alert alert-primary" id="alert"role="alert">
                 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



        