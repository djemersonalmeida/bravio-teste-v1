@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Weather in my city</div>
                <div class="card-body">
                 @include('layouts.alerts')
                    {{Form::open(array('route' => array('check_weather')))}}
                            <div class="form-group">
                                <input type="text" class="form-control" name="city"  placeholder="Digite uma Cidade">
                            </div>
                            <input type="submit" class="btn btn-primary btn-block" value="Ver tempo">
                    {{ Form::close() }}
                </div>
                
                <div class="form-group">
                @if($data->cod == '200')
                    <div class="alert alert-success" id="alert"role="alert">
                    City: {{$data->name}} </br>
                    Weather: {{$data->main->temp}} Fahrenheits</br>
                    Min. Temp: {{$data->main->temp_min}} Fahrenheits</br>
                    Max. Temp: {{$data->main->temp_max}} Fahrenheits</br>
                    </div>
                  @else
                   <div class="alert alert-danger" id="alert"role="alert">
                    {{$data->message}}
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



        